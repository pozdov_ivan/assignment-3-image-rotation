#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/bmp.h" // idk
#include "../include/image.h"

#define BMP_SIGNATURE 0x4D42

int read_bmp_file(char *in, size_t size, struct IMAGE *img)
{
    struct BMP_HEADER header = *(struct BMP_HEADER *)in;

    if (header.bfType != BMP_SIGNATURE)
    {
        fprintf(stderr, "Not a BMP file\n");
        return 1;
    }

    img->height = header.biHeight;
    img->width = header.biWidth;
    size_t pixel_data_size = size - header.bfOffBits;
    img->data = malloc(pixel_data_size);
    if (!img->data)
    {
        fprintf(stderr, "Failed to allocate memory to pixel data\n");
        return 1;
    }

    in += header.bfOffBits;
    size_t offset = 0;
    size_t padding = 4 - ((img->width * 3) % 4);
    for (size_t current_height = 0; current_height < img->height; current_height++)
    {
        for (size_t current_width = 0; current_width < img->width; current_width++)
        {
            struct PIXEL p =
                {
                    .b = in[offset],
                    .g = in[offset + 1],
                    .r = in[offset + 2]};

            img->data[current_width + img->width * current_height] = p;
            offset += 3;
        }
        // add padding
        offset += padding;
    }

    return 0;
}

int write_bmp_file(char **out, const struct IMAGE *img, uint32_t *output_file_size)
{
    uint32_t padding = (4 - (img->width * 3) % 4); // zzz
    uint32_t image_data_size = (uint32_t)(img->height * (img->width * 3 + padding));
    uint32_t file_size = image_data_size + (uint32_t)sizeof(struct BMP_HEADER);
    *output_file_size = file_size;

    struct BMP_HEADER header =
        {
            .bfType = BMP_SIGNATURE,
            .bfileSize = file_size,
            .bfReserved = 0,
            .bfOffBits = sizeof(struct BMP_HEADER),
            .biSize = 40,
            .biWidth = (uint32_t)img->width,
            .biHeight = (uint32_t)img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image_data_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0};

    char *temp_mem = calloc(file_size, 1);
    if (!temp_mem)
    {
        fprintf(stderr, "Failed to allocate memory for bmp file after rotating\n");
        return 1;
    }

    *(struct BMP_HEADER *)temp_mem = header;
    char *bmp_file_data = temp_mem + sizeof(header);

    size_t offset = 0;
    for (size_t current_height = 0; current_height < img->height; current_height++)
    {
        for (size_t current_width = 0; current_width < img->width; current_width++)
        {
            *(struct PIXEL *)&bmp_file_data[offset] = img->data[current_height * img->width + current_width];
            offset += 3;
        }
        offset += padding;
    }
    *out = temp_mem;
    return 0;
}
