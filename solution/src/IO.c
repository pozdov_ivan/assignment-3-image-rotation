#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

char *read_file_from_path(const char *path, size_t *file_size)
{
    FILE *file = fopen(path, "rb");
    if (!file)
    {
        fprintf(stderr, "Failed to open file for reading: %s\n", path);
        exit(1);
    }

    fseek(file, 0, SEEK_END);
    size_t size = ftell(file);
    fseek(file, 0, SEEK_SET);

    char *buffer = malloc(size);
    if (!buffer)
    {
        fprintf(stderr, "Failed to allocate memory\n");
        fclose(file);
        exit(1);
    }
    fread(buffer, 1, size, file);

    *file_size = size;
    fclose(file);
    return buffer;
}

void write_file_to_path(const char *path, char *out, uint32_t output_file_size)
{
    FILE *file = fopen(path, "wb");
    if (!file)
    {
        fprintf(stderr, "Failed to open file for writing: %s\n", path);
        exit(1);
    }

    size_t bytes_written = fwrite(out, 1, output_file_size, file);
    if (bytes_written != output_file_size)
    {
        fprintf(stderr, "Error writing to file: %s\n", path);
        fclose(file);
        exit(1);
    }

    fclose(file);
}
