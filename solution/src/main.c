#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "IO.h"
#include "image.h"
#include "rotate.h"

int main(int argc, char **argv)
{
    if (argc != 4)
    {
        fprintf(stderr, "Incorrect amount of arguments. Expected: 3\n Usage: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return 1;
    }

    const char *source_image_path = argv[1];
    const char *transformed_image_path = argv[2];
    const int angle = atoi(argv[3]);

    size_t file_size;
    char *source_image = read_file_from_path(source_image_path, &file_size);

    struct IMAGE image;
    int status = read_bmp_file(source_image, file_size, &image);
    free(source_image); // free since we have another formatless copy
    if (status)
    {
        exit(1);
    }

    rotate_image_by_degrees(angle, &image);

    char *out = NULL;
    uint32_t output_file_size = 0;
    status = write_bmp_file(&out, &image, &output_file_size);
    free((&image)->data);
    if (status)
    {
        exit(1);
    }

    write_file_to_path(transformed_image_path, out, output_file_size);
    free(out);

    return 0;
}
