#include <stdint.h>
#include <stdlib.h>

#include "../include/image.h"

void rotate_180(struct IMAGE *img)
{
    struct PIXEL *data = img->data;
    struct PIXEL temp;

    for (size_t i = 0; i < img->height / 2; i++)
    {
        for (size_t j = 0; j < img->width; j++)
        {
            temp = data[i * img->width + j];
            data[i * img->width + j] = data[(img->height - 1 - i) * img->width + j];
            data[(img->height - 1 - i) * img->width + j] = temp;
        }
    }

    for (size_t i = 0; i < img->height; i++)
    {
        for (size_t j = 0; j < img->width / 2; j++)
        {
            temp = data[i * img->width + j];
            data[i * img->width + j] = data[img->width * i + img->width - 1 - j];
            data[img->width * i + img->width - 1 - j] = temp;
        }
    }
}

void rotate_90_counter_clockwise(struct IMAGE *img)
{
    struct PIXEL *data = img->data;

    struct IMAGE new_img;
    new_img.height = img->width;
    new_img.width = img->height;

    new_img.data = malloc(sizeof(struct PIXEL) * new_img.height * new_img.width);

    for (size_t i = 0; i < img->height; i++)
    {
        for (size_t j = 0; j < img->width; j++)
        {
            size_t new_i = j;
            size_t new_j = img->height - i - 1;

            new_img.data[new_i * new_img.width + new_j] = data[i * img->width + j];
        }
    }

    free(img->data);
    *img = new_img;
}

void rotate_image_by_degrees(const int deg, struct IMAGE *img)
{
    switch (deg)
    {
    case 180:
    case -180:
        rotate_180(img);
        break;
    case -90:
    case 270:
        rotate_90_counter_clockwise(img);
        break;
    case 90:
    case -270:
        rotate_180(img);
        rotate_90_counter_clockwise(img);
        break;
    case 0:
        return;
        break;
    default:
        break;
    }
}
