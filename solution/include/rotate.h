#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"

void rotate_180(struct IMAGE *img);
void rotate_image_by_degrees(const int deg, struct IMAGE *img);
void rotate_90_counter_clockwise(struct IMAGE *img);

#endif
