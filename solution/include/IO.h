#ifndef LOCAL_IO_H
#define LOCAL_IO_H

#include <stdio.h>

char *read_file_from_path(const char *path, size_t *file_size);
void *write_file_to_path(const char *path, char *out, uint32_t output_file_size);

#endif
