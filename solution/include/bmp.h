#ifndef BMP_H
#define BMP_H

#include <stdint.h>
#include <stdio.h>

#include "image.h"

#pragma pack(push, 1) // tell compiler to remove padding between struct members i guess
struct BMP_HEADER
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bfOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

int read_bmp_file(char *in, size_t size, struct IMAGE *img);

int write_bmp_file(char **out, const struct IMAGE *img, uint32_t *output_file_size);

#endif
