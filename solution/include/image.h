#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

struct IMAGE
{
    uint64_t width, height;
    struct PIXEL *data;
};

struct PIXEL
{
    uint8_t b, g, r;
};

#endif
